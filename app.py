import flask
import json
app = flask.Flask(__name__)

@app.route("/")
def ola():
    mensagem = "Bem vindos ao curso de Python"
    return flask.render_template ("index.html",mensagem=mensagem)

@app.route("/upper", methods=['POST'])
def upper():
    dados_json = flask.request.get_json()
    mensagem_upper = dados_json['mensagem'].upper()
    return  { "mensagem": mensagem_upper }

app.run(host='0.0.0.0', port=80)