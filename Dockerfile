
FROM python:3.8.5-alpine


copy . app

WORKDIR /app

RUN pip install flask

CMD ["python", "app.py"]